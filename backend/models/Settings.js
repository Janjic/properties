import mongoose from 'mongoose';
const Schema = mongoose.Schema;
let Settings = new Schema({
    tag_name: {
      type: String
    },
    color: {
      type: String
    },
    height: {
      type: String
    },
    width: {
      type: String
    },
    border_width: {
      type: String
    },
    border_radius: {
      type: String
    },
    background_color: {
      type: String
    },
    font_size: {
      type: String
    },
    font_weight: {
      type: Number
    },
    padding: {
      type: String
    }
});
export default mongoose.model('Settings', Issue);
