import mongoose from 'mongoose';
const Schema = mongoose.Schema;
let Issue = new Schema({
    /*title: {
        type: String
    },
    responsible: {
        type: String
    },
    description: {
        type: String
    },
    severity: {
        type: String
    },
    status: {
        type: String,
        default: 'Open'
    }*/

    tag_name: {
      type: String
    },
    color: {
      type: String
    },
    height: {
      type: String
    },
    width: {
      type: String
    },
    border_width: {
      type: String
    },
    border_radius: {
      type: String
    },
    background_color: {
      type: String
    },
    font_size: {
      type: String
    },
    font_weight: {
      type: Number
    },
    padding: {
      type: String
    }
});
export default mongoose.model('Issue', Issue);
