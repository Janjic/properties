import { Component, OnInit, Input } from '@angular/core';
import { Property } from "../property";
import { PropService } from "../prop.service";

@Component({
  selector: 'app-form-view',
  templateUrl: './form-view.component.html',
  styleUrls: ['./form-view.component.css']
})
export class FormViewComponent implements OnInit {

  @Input() prope:Property;


  constructor(private propService: PropService) { }

  sendData(prope:Property): void {
    this.propService.addProperty(prope).subscribe();
  }

  ngOnInit() {

  }

}
