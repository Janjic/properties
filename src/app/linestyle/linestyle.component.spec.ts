import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinestyleComponent } from './linestyle.component';

describe('LinestyleComponent', () => {
  let component: LinestyleComponent;
  let fixture: ComponentFixture<LinestyleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LinestyleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinestyleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
