import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CpickerComponent } from './cpicker.component';

describe('CpickerComponent', () => {
  let component: CpickerComponent;
  let fixture: ComponentFixture<CpickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CpickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CpickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
