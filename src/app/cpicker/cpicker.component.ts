import { Component, OnInit, Input } from '@angular/core';
import { Property } from "../property";

@Component({
  selector: 'app-cpicker',
  templateUrl: './cpicker.component.html',
  styleUrls: ['./cpicker.component.css']
})
export class CpickerComponent implements OnInit {

  @Input() prope:Property;

  constructor() { }

  ngOnInit() {
  }

}
