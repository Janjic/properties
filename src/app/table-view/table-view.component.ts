import { Component, OnInit, Input } from '@angular/core';
import { Property } from "../property";

@Component({
  selector: 'app-table-view',
  templateUrl: './table-view.component.html',
  styleUrls: ['./table-view.component.css']
})
export class TableViewComponent implements OnInit {

  //properties: Property[] = PROPERTIES;
  @Input() propet:Property;

  constructor() { }

  ngOnInit() {
  }

}
