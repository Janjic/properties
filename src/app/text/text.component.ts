import { Component, OnInit, Input } from '@angular/core';
import { Property } from "../property";

@Component({
  selector: 'app-text',
  templateUrl: './text.component.html',
  styleUrls: ['./text.component.css']
})
export class TextComponent implements OnInit {

  @Input() prope:Property;

  constructor() { }

  ngOnInit() {
  }

}
