[
	{
		objectType: 'Symbol',
		settings: [
			{
				name: 'name',
				title: 'Name',
				type: 'Text',
				defaultValue: null
			},
			{
				name: 'title',
				title: 'Title',
				type: 'Text',
				defaultValue: null
			},
			{
				name: 'titlePosition',
				title: 'Title Position',
				type: 'Position',
				defaultValue: 'center'
			},
			{
				name: 'font',
				title: 'Font',
				type: 'Font',
				defaultValue: 'Times New Roman'
			},
			{
				name: 'fontSize',
				title: 'Font Size',
				type: 'Integer',
				minValue: 8,
				maxValue: 40,
				defaultValue: 24
			},
			{
				name: 'backgroundColor',
				title: 'Background Color',
				type: 'Color',
				defaultValue: '#aaaaaa'
			},
			{
				name: 'roundedConers',
				title: 'Rounded Corners',
				type: 'Boolean',
				defaultValue: false
			}
		]
	},
	{
		objectType: 'Connection',
		settings: [
			{
				name: 'name',
				title: 'Name',
				type: 'Text',
				defaultValue: null
			},
			{
				name: 'lineColor',
				title: 'Line Color',
				type: 'Color',
				defaultValue: '#000000'
			},
			{
				name: 'arrowStart',
				title: 'Show Arrow at Start',
				type: 'Boolean',
				defaultValue: false
			},
			{
				name: 'arrowEnd',
				title: 'Show Arrow at End',
				type: 'Boolean',
				defaultValue: true
			},
			{
				name: 'lineStyle',
				title: 'Line Style',
				type: 'LineStyle',
				defaultValue: 'solid'
			}
		]
	},
	{
		objectType: 'Grouping',
		settings: [
			{
				name: 'name',
				title: 'Name',
				type: 'Text',
				defaultValue: null
			},
			{
				name: 'backgroundColor',
				title: 'Background Color',
				type: 'Color',
				defaultValue: '#000066'
			},
			{
				name: 'locked',
				title: 'Lock Group',
				type: 'Boolean',
				defaultValue: false
			},
			{
				name: 'borderStyle',
				title: 'Border Style',
				type: 'LineStyle',
				defaultValue: 'dotted'
			}
		]
	}
]
