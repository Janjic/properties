import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders }from '@angular/common/http';
import { Property } from "./property";
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

@Injectable()
export class PropService {

  private url = "http://localhost:4000/issues/add";
  private url_one = "http://localhost:4000/issues/";

  addProperty(prope: Property): Observable<any> {
      //delete(prope._id);
    return this.http.post<any>(this.url, prope);
  }

  getOneProperty(id:string): Observable<any>{
    return this.http.get<any>(`${this.url_one}${id}`);
  }

  getAll():Observable<any>{
    return this.http.get<any>(this.url);
  }

  constructor(private http: HttpClient) { }

}
