import { Component } from '@angular/core';
import { Property } from './property';
import { PropService } from "./prop.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {

  property: Property;
  propertiesDB: Property[];

  constructor(private propService: PropService) { }

  getOne(id:string): any {
    this.propService.getOneProperty(id).subscribe(data => this.property = data);
  }

  getAllP():any{
    return this.propService.getAll().subscribe(data => this.propertiesDB = data);
  }

  ngOnInit(): void {
  this.getOne('5b6a17a2cfb7571eb071bf94'); //id
}

  /*property: Property =
    { tag_name: "div",
      color: "#ffff64",
      height: "300px",
      width: "300px",
      border_width: "3px",
      border_radius: "5px",
      background_color: "#f5ad34",
      font_size: "40px",
      font_weight: 600,
      padding: "20px"}*/
};
