export class Setting {
  name: string;
  title: string;
  type: string;
  defaultValue: string;
}
