export class Property {
  tag_name: string;
  color: string;
  height: string;
  width: string;
  border_width: string;
  border_radius: string;
  background_color: string;
  font_size: string;
  font_weight: number;
  padding: string;
}
