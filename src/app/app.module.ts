import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { TableViewComponent } from './table-view/table-view.component';
import { LiveViewComponent } from './live-view/live-view.component';
import { FormViewComponent } from './form-view/form-view.component';

import { FormsModule } from '@angular/forms';
import { ColorPickerModule } from 'ngx-color-picker';
import { HttpClientModule } from '@angular/common/http';
import { PropService } from './prop.service';
import { CpickerComponent } from './cpicker/cpicker.component';
import { TextComponent } from './text/text.component';
import { NumberComponent } from './number/number.component';
import { LinestyleComponent } from './linestyle/linestyle.component';
import { PositionComponent } from './position/position.component';
import { BooleanComponent } from './boolean/boolean.component';
import { FontComponent } from './font/font.component';


@NgModule({
  declarations: [
    AppComponent,
    TableViewComponent,
    LiveViewComponent,
    FormViewComponent,
    CpickerComponent,
    TextComponent,
    NumberComponent,
    LinestyleComponent,
    PositionComponent,
    BooleanComponent,
    FontComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ColorPickerModule,
    HttpClientModule
  ],
  providers: [PropService],
  bootstrap: [AppComponent]
})
export class AppModule { }
