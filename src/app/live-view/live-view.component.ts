import { Component, OnInit, Input } from '@angular/core';
import { Property } from "../property";

@Component({
  selector: 'app-live-view',
  templateUrl: './live-view.component.html',
  styleUrls: ['./live-view.component.css']
})
export class LiveViewComponent implements OnInit {

  @Input() prope:Property;

  constructor() { }

  ngOnInit() {
  }

}
